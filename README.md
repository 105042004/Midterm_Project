# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* FORUM
* Key functions (add/delete)
    1. home page
    2. sing in page
    3. post list page
    4. user page
* Other functions (add/delete)
    1. delete comment
    2. sign in with google
    3. CSS animation 
    4. RWD
    5. notification

## Basic Components
|Component|Score|Y/N| 
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|
|Other functions|1~10%|Y|

## Website Detail Description
index.html畫面為home頁面，使用者登入前沒有辦法顯示任何資料。
按右上角的log in 可以進入登入畫面，並且可以進行帳號登入，申請帳號，或google登入。
登入後跳回home頁面，並且可觀看資料庫中所有post。
此外製作獨立的'my page'讓登入的使用者看到自己的post，也可以在該頁刪除post

作品網址：https://ss2018-midterm-project.firebaseapp.com/

報告網址：https://gitlab.com/105042004/Midterm_Project/blob/master/README.md
## Security Report (Optional)
